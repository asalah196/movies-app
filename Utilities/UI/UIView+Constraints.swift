//
//  UIView+Constraints.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/9/22.
//

import Foundation

import UIKit
public extension UIView {
    /// Pin the UIView to the needed place
    /// - Parameters:
    ///   - topAnchor: _Relevant anchor_ that the top anchor should be distanced from
    ///   - leftAnchor: _Relevant anchor_ that the leading anchor should be distanced from
    ///   - bottomAnchor: _Relevant anchor_ that the bottom anchor should be distanced from
    ///   - rightAnchor: _Relevant anchor_ that the trainling anchor should be distanced from
    ///   - left: Distance from left anchor passed. Defaults to 0
    ///   - top: Distance from top anchor passed. Defaults to 0
    ///   - bottom: Distance from bottom anchor passed. Defaults to 0
    ///   - right: Distance from right anchor passed. Defaults to 0
    func pin(topAnchor: NSLayoutYAxisAnchor?=nil,
             leftAnchor: NSLayoutXAxisAnchor?=nil,
             bottomAnchor: NSLayoutYAxisAnchor?=nil,
             rightAnchor: NSLayoutXAxisAnchor?=nil,
             left: CGFloat=0,
             top: CGFloat=0,
             bottom: CGFloat=0,
             right: CGFloat=0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        var constraints = [NSLayoutConstraint]()
        if let topAnchor = topAnchor {
            constraints.append(self.topAnchor.constraint(equalTo:topAnchor, constant: top))
        }
        if let leftAnchor = leftAnchor {
            constraints.append(self.leadingAnchor.constraint(equalTo:leftAnchor, constant: left))
        }
        if let bottomAnchor = bottomAnchor {
            constraints.append(self.bottomAnchor.constraint(equalTo:bottomAnchor, constant: bottom))
        }
        if let rightAnchor = rightAnchor {
            constraints.append(self.trailingAnchor.constraint(equalTo:rightAnchor, constant: right))
        }
        NSLayoutConstraint.activate(constraints)
    }

    func centralizeInParent(xPadding: CGFloat = 0,
                            yPadding: CGFloat = 0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            self.centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: xPadding).isActive = true
            self.centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: yPadding).isActive = true
        }
    }

    /// Adds views as subviews
    /// - Parameter views: views to be added
    func addSubViews(views: UIView...){
        views.forEach { view in
            addSubview(view)
        }
    }
}
