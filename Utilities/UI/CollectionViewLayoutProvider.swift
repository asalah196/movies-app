//
//  CollectionViewLayoutProvider.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/11/22.
//

import UIKit

public enum CollectionViewLayoutProvider {
    /// ViewLayout to be used by the compositional collection view
    public static func createLayouts() -> UICollectionViewLayout {
        UICollectionViewCompositionalLayout { sectionIndex, _ in

            switch sectionIndex {
                // The movie details cell configuration
            case 0:
                let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)))
                item.contentInsets.trailing = 0
                item.contentInsets.leading = 0
                item.contentInsets.bottom = 0
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(350)), subitems: [item])
                let section = NSCollectionLayoutSection(group: group)
                section.orthogonalScrollingBehavior = .none
                let headerKind = UICollectionView.elementKindSectionHeader
                section.boundarySupplementaryItems = [.init(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(40)), elementKind: headerKind, alignment: .topLeading)]
                return section
                // The preview cell configuration
            case 1:
                let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(0.90), heightDimension: .absolute(150)), subitems: [item])
                let section = NSCollectionLayoutSection(group: group)
                section.contentInsets.top = 10
                section.contentInsets.bottom = 10
                section.contentInsets.trailing = 10
                section.orthogonalScrollingBehavior = .continuous
                let headerKind = UICollectionView.elementKindSectionHeader
                section.boundarySupplementaryItems = [.init(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(40)), elementKind: headerKind, alignment: .topLeading)]
                return section
            default:
                fatalError()
            }
        }
    }
}
