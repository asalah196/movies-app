//
//  UITableViewCell+defaultReuseIdentifier.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/11/22.
//

import UIKit

public extension UITableViewCell {

    /// The class name as String
    static var reuseID: String {
        return String(describing: self)
    }

    /// A nib with the same name as the Class
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
}
