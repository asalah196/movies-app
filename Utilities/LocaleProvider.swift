//
//  LocaleProvider.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/14/22.
//

import Foundation

public class LocaleProvider {
    public static var currentLocale: String {
        Locale.current.languageCode ?? "en"
    }
}
