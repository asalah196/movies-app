//
//  DispatchQueue+mainAsyncSafe.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/12/22.
//

import Foundation

public extension DispatchQueue {


    /// Executes a block of code on the main queue
    /// Dispatches to the main queue only if the current queue is not the main
    /// - Parameter block: block to be executed
    class func safeAsyncMain(block: @escaping () -> Void){
        if Thread.isMainThread {
            block()
        } else {
            DispatchQueue.main.async {
                block()
            }
        }
    }
}
