//
//  APIClient.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation
import Alamofire

enum Result {
    case success, failure
}

public protocol ResponseProtocol {
    typealias RequestCompletionHandler<T> = (_ response: T?, _ error: Error?) -> Void
}

public class APIClient: ResponseProtocol {

    @discardableResult
    public static func performRequest<T: Decodable>(route:APIRouter, completion:@escaping RequestCompletionHandler<T>) -> DataRequest? {

        return AF.request(route).validate().responseDecodable(of: T.self) { response in
            switch response.result {
            case .success(let data):
                completion(data, nil)
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
}
