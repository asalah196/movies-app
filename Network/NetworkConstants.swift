//
//  Constants.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation

private struct UrlProviderConstants {
    static let apiToken = "c9856d0cb57c3f14bf75bdc6c063b8f3"
    static let apiBaseUrl = "https://api.themoviedb.org/3/"
    static let imagesBaseUri = "https://image.tmdb.org/t/p"

    static let searchUri = "search/movie"
    static let popularUri = "discover/movie"
    static let detailsUri = "movie/"
    static let similarMoviesUri = "movie/%d/similar"

}

public struct URLProvider {

    static var apiToken: String {
        get {
            return UrlProviderConstants.apiToken
        }
    }
    static var searchUri: String {
        get {
            return UrlProviderConstants.apiBaseUrl.appending(UrlProviderConstants.searchUri)
        }
    }
    static var popularUri: String {
        get {
            return UrlProviderConstants.apiBaseUrl.appending(UrlProviderConstants.popularUri)
        }
    }

    public static func imageURL(with path: String, size: String) -> URL {
        let baseUri = URL(string: UrlProviderConstants.imagesBaseUri)!
        let url = baseUri.appendingPathComponent(size)
        return url.appendingPathComponent(path)
    }

    static func detailsURL(id: Int) -> String {
        return UrlProviderConstants.apiBaseUrl.appending(UrlProviderConstants.detailsUri).appending("\(id)")
    }

    static func similarMoviesURL(id: Int) -> String {
        let urlPath = String(format: UrlProviderConstants.similarMoviesUri, id)
        return UrlProviderConstants.apiBaseUrl.appending(urlPath)
    }
}

struct RequestParameters {
    static let api_key = "api_key"
    static let page = "page"
    static let posterPath = "poster_path"
    static let results = "results"
    static let id = "id"
    static let title = "title"
    static let query = "query"
    static let overview = "overview"
    static let language = "language"
    static let releaseDate = "release_date"
    static let totalPages = "total_pages"
}
