//
//  APIRouter.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation
import Alamofire
import Utilities

public enum APIRouter: URLRequestConvertible {
    case fetchMovies(query: String, page: Int)
    case popularMovies(page: Int)
    case movieDetails(id: Int)
    case similarMovies(to: Int)
    private var method: HTTPMethod{
        switch self {
        case .fetchMovies, .popularMovies, .movieDetails, .similarMovies:
            return .get
        }
    }

    private var path: String {
        switch self {
        case .fetchMovies:
            return URLProvider.searchUri
        case .popularMovies:
            return URLProvider.popularUri
        case .movieDetails(let id):
            return URLProvider.detailsURL(id: id)
        case .similarMovies(let id):
            return URLProvider.similarMoviesURL(id: id)
        }
    }

    private var parameters: Parameters? {
        switch self {
        case .fetchMovies(let query, let page):
            return [RequestParameters.api_key: URLProvider.apiToken,
                    RequestParameters.language: LocaleProvider.currentLocale,
                    RequestParameters.page: page,
                    RequestParameters.query: query]
        case .popularMovies(page: let page):
            return [RequestParameters.api_key: URLProvider.apiToken,
                    RequestParameters.language: LocaleProvider.currentLocale,
                    RequestParameters.page: page]
        default:
            return [RequestParameters.api_key: URLProvider.apiToken,
                    RequestParameters.language: LocaleProvider.currentLocale
            ]
        }
    }

    public func asURLRequest() throws -> URLRequest {
        var url = try path.asURL()
        // Load local file
        if ProcessInfo().arguments.contains("test_mode") {
            url = Bundle.main.url(forResource: "movies", withExtension: "json")!
        }
        var request = URLRequest(url: url)

        request.httpMethod = method.rawValue
        let encoding = URLEncoding.default

        return try encoding.encode(request, with: parameters)
    }

}
