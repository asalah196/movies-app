//
//  MoviesAppUITests.swift
//  MoviesAppUITests
//
//  Created by Ahmed Salah on 12/8/22.
//

import XCTest

class MoviesAppUITests: XCTestCase {

    var app: XCUIApplication!
    override func setUpWithError() throws {
        app = XCUIApplication()
        app.launchArguments = ["test_mode"]
        app.launch()
    }

    func testFetchingResult() throws {
        let titleText = app.staticTexts["Black Adam"]
        let releaseDataText = app.staticTexts["2022-10-19"]
        XCTAssertTrue(titleText.waitForExistence(timeout: 2.0))
        XCTAssertTrue(releaseDataText.exists)
    }

    func testFetchCount() throws {
        let moviesTable = app.tables.firstMatch
        XCTAssertTrue(moviesTable.waitForExistence(timeout: 2.0))
        XCTAssertEqual(moviesTable.cells.count, 21)
    }

}
