# Movies App

## Overview

An app that is built using MVVM that fetches the trending movies from [the moviesdb API](https://developers.themoviedb.org/3/discover/moviediscoverer/movie-discover) and display it in a user-friendly interface.

I have developed this app using a combination of .xibs and code-developed UI as I didn't know which stack is required for the task

## Features
- User can search with a specific text
- When a movie is selected all this movie details are displayed in a new screen
- In addition to the movie details, a list of similar movies are displayed

## What else

- Low data mode is respected by loading lower quality pirctures
- Unit tests and UI tests
- The app supports English and Arabic locales
- Modularized app as code is split into multiple frameworks e.g. Network

## Pods
- Alamofire for networking
- KingFisher for loading/caching images
- OHHTTPS for stubbing requests in tests
