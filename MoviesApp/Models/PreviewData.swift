//
//  PreviewData.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation

struct PreviewData {
    let id: Int?
    let title: String?
    let imageURL: URL?
    let lowDataImageURL: URL?
    let releaseDate: String?
    let overview: String?
}
