//
//  ImageSize.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation
enum ImageSize {
    case w500
    case original

    func string() -> String {
        switch self {
        case .w500:
            return "/w500"
        case .original:
            return "/original"
        }
    }
}
