//
//  MyAlert.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/12/22.
//

import UIKit

class MyAlert: UIAlertController {
    static let shared = MyAlert()
    private var alert: UIAlertController?

    public func getAlert( _ title: String, message: String, preferredStyle: UIAlertController.Style = .alert) -> UIAlertController? {
        guard !isBeingPresented else { return nil}
        alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert!.addAction(okAction)
        return alert
    }

}
