//
//  MovieDetails.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation
import Network

struct MovieDetails: Decodable {

    var id: Int
    var title: String

    private var posterPath: String?
    private var fallbackPath: String?

    var overview: String?
    var releaseDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case releaseDate = "release_date"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey:.id)
        title = try container.decode(String.self, forKey:.title)
        posterPath = try? container.decode(String.self, forKey:.posterPath)
        fallbackPath = try? container.decode(String.self, forKey:.backdropPath)
        releaseDate = try? container.decode(String.self, forKey:.releaseDate)
        overview = try? container.decode(String.self, forKey:.overview)
    }
}

extension MovieDetails {

    func getImageURL(with size: ImageSize = .original) -> URL? {
        guard let path = posterPath ?? fallbackPath else {
            return nil
        }
        return URLProvider.imageURL(with: path, size: size.string())
    }
    
}

extension MovieDetails {
    var previewData: PreviewData {
        PreviewData (id: id ,title: title, imageURL: getImageURL(), lowDataImageURL: getImageURL(with: .w500), releaseDate: releaseDate, overview: overview)
    }
}


