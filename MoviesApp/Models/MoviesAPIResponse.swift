//
//  MoviesAPIResponse.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation

struct MoviesAPIResponse: Decodable {
    var page: Int?
    var totalPages: Int?
    var results: [MovieDetails]?

    enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalPages = "total_pages"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        page = try container.decode(Int.self, forKey:.page)
        totalPages = try container.decode(Int.self, forKey:.totalPages)
        results = try? container.decode([MovieDetails].self, forKey:.results)
    }
}
