//
//  String+Year.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
