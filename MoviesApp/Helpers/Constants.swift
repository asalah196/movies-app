//
//  Constants.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/14/22.
//

import Foundation

let kSearchBarTitleKey = "search_bar_placeholder"
let kMovieDetailsHeaderKey = "movie_details_header"
let kSimilarMoviesHeaderKey = "similar_movies_header"
let kNetworkErrorTitleKey = "network_error_title"
let kNetworkErrorMessageKey = "network_error_details_fallback"
let kDefaultImageName = "MovieClapper"
