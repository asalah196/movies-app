//
//  ViewController.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/8/22.
//

import UIKit
import Utilities

class SearchTableViewController: UITableViewController {
    private let loadingCellHeight: CGFloat = 80.0
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    var viewModel: MoviesViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModel = MoviesViewModel(delegate: self)
        viewModel.search(forced: true)
    }

    func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = kSearchBarTitleKey.localized
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.shouldLoad(movieOrder: indexPath.row) {
            return loadingCellHeight
        }
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // +1 for the loading cell
        return viewModel.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if viewModel.shouldLoad(movieOrder: indexPath.row) {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadingCell.reuseID, for: indexPath) as! LoadingCell
            viewModel.loadMore()
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.reuseID, for: indexPath) as! MovieCell
        let movie = viewModel.movie(at: indexPath.row)
        cell.setMovie(movie)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = MovieDetailsCollectionViewController()
        let movieId = viewModel.movie(at: indexPath.row).id
        detailsVC.movieID = movieId
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.backgroundColor = .systemGray5
        let header = view as! UITableViewHeaderFooterView
        header.backgroundColor = .systemGray5
        header.textLabel?.textColor = .systemGray
    }

    func setupUI() {
        tableView.register(MovieCell.nib, forCellReuseIdentifier: MovieCell.reuseID)
        tableView.register(LoadingCell.nib, forCellReuseIdentifier: LoadingCell.reuseID)
        tableView.backgroundColor = .systemGray5
        setupSearchBar()
    }
}

extension SearchTableViewController: FetchRequestDelegate {
    func fetchDidSucceed() {
        DispatchQueue.safeAsyncMain { [weak tableView] in
            tableView?.reloadData()
        }
    }
    func fetchDidFail() {
        if let alert = MyAlert.shared.getAlert(kNetworkErrorTitleKey.localized, message: kNetworkErrorMessageKey.localized){
            DispatchQueue.safeAsyncMain { [unowned self] in
                self.present(alert, animated: true)
            }
        }
    }
}

extension SearchTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text
        viewModel.search(for: text)
    }
}
