//
//  MovieTableViewCell.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 5/21/22.
//

import UIKit
import Kingfisher

class MovieCell: UITableViewCell {


    @IBOutlet fileprivate weak var title: UILabel!
    @IBOutlet fileprivate weak var overview: UILabel!
    @IBOutlet fileprivate weak var releaseDate: UILabel!
    @IBOutlet fileprivate weak var posterImage: UIImageView! {
        didSet {
            if let imageView = posterImage {
                imageView.kf.indicatorType = .activity
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        styleSetup()
    }
    
    func setMovie(_ movie: PreviewData) {
        self.title.text = movie.title
        self.overview.text = movie.overview
        self.releaseDate.text = movie.releaseDate
        if let url = movie.imageURL {
            posterImage.kf.setImage(with: url)
        }
    }

}

extension MovieCell {
    
    func styleSetup() {
        backgroundColor = .systemGray5
        posterImage.layer.borderWidth = 0.3
        posterImage.layer.cornerRadius = 10
    }
    
}
