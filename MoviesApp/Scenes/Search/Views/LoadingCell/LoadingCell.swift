//
//  LoadingCell.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 5/21/22.
//

import UIKit

class LoadingCell: UITableViewCell {

    override func awakeFromNib() {
        backgroundColor = .systemGray5
        selectionStyle = .none
    }

}
