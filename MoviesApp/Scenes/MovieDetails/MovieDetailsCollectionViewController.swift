//
//  MovieDetailsCollectionViewController.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/11/22.
//

import UIKit
import Utilities

class MovieDetailsCollectionViewController: UIViewController {

    private var collectionView: UICollectionView!
    private var movieDetails: PreviewData?
    private var movieDetailsView = MovieDetailsCell()

    var movieID: Int!
    var viewModel: DetailsViewModel!

    let sectionsNames = [ kMovieDetailsHeaderKey.localized,
                          kSimilarMoviesHeaderKey.localized]

    var similarMoviesList: [PreviewData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        registerViews()
        viewModel = DetailsViewModel(delegate: self)
        viewModel.fetchMovieDetails(for: movieID)
        viewModel.fetchSimilarMovies(to: movieID)
    }

    func configureCollectionView() {
        let collectionView = UICollectionView(frame: view.bounds,
                                              collectionViewLayout: CollectionViewLayoutProvider.createLayouts())
        view.addSubview(collectionView)
        collectionView.backgroundColor = .systemGray6
        collectionView.dataSource = self
        self.collectionView = collectionView
    }

}

extension MovieDetailsCollectionViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        sectionsNames.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }

        return similarMoviesList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.section == 0) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieDetailsCell.reuseID, for: indexPath) as! MovieDetailsCell
            guard let movieDetails = movieDetails else {return cell}
            cell.setup(movie: movieDetails)

            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SimilarMovieCell.reuseID, for: indexPath) as! SimilarMovieCell
            cell.setup(cellInfo: similarMoviesList[indexPath.row])
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SectionHeaderView.reuseId, for: indexPath) as! SectionHeaderView
        header.text = sectionsNames[indexPath.section]
        return header
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        collectionView.frame = view.bounds
        collectionView.reloadData()
    }
}

extension MovieDetailsCollectionViewController: DetailsViewModelDelegate {
    
    func viewModel(didFetch similarMovies: [PreviewData]) {
        self.similarMoviesList = similarMovies
        DispatchQueue.safeAsyncMain { [weak self] in
            self?.collectionView.reloadData()
        }
    }

    func viewModel(didFailWith error: Error?) {
        if let alert = MyAlert.shared.getAlert(kNetworkErrorTitleKey.localized, message:error?.localizedDescription ?? kNetworkErrorMessageKey.localized){
            DispatchQueue.safeAsyncMain { [unowned self] in
                self.present(alert, animated: true)
            }
        }
    }

    func viewModel(didFetch movieDetails: PreviewData) {
        self.movieDetails = movieDetails
        DispatchQueue.safeAsyncMain { [unowned self] in
            self.movieDetailsView.setup(movie: movieDetails)
            self.collectionView.reloadData()
        }
    }
}

extension MovieDetailsCollectionViewController {
    func registerViews(){
        collectionView.register(SimilarMovieCell.self, forCellWithReuseIdentifier: SimilarMovieCell.reuseID)
        collectionView.register(MovieDetailsCell.self, forCellWithReuseIdentifier: MovieDetailsCell.reuseID)
        collectionView.register(SectionHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SectionHeaderView.reuseId)
    }

    func showAlert(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }
}
