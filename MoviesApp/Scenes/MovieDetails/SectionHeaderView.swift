//
//  SectionHeaderView.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/12/22.
//

import UIKit
import Utilities

class SectionHeaderView: UICollectionReusableView {
    static var reuseId = "HeaderView"

    let label: TitleLabel = {
        let label = TitleLabel(textSize: 20, color: .systemGray)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var text: String = "" {
        didSet {
            label.text = text
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        label.pin(topAnchor: topAnchor, leftAnchor: leadingAnchor, bottomAnchor: bottomAnchor, rightAnchor: trailingAnchor, left: 10, top: 5)

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
