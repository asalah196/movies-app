//
//  SimilarMovieCell.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/12/22.
//

import UIKit
import Utilities

class SimilarMovieCell: UICollectionViewCell {

    static var reuseID = String(describing: SimilarMovieCell.self)

    lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.layer.cornerRadius = 10
        image.contentMode = .scaleAspectFill
        return image
    }()

    let view = UIView()

    let titleLabel = TitleLabel(textSize: 20, color: .systemYellow, alignment: .center)

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    func setup(cellInfo: PreviewData){
        imageView.kf.indicatorType = .activity
        if let imageURL = cellInfo.imageURL,
           let lowDataURL = cellInfo.lowDataImageURL{
            imageView.kf.setImage(with: imageURL,
                                  options: [.lowDataMode(.network(lowDataURL))])
        } else {
            imageView.image = UIImage(named: kDefaultImageName)
        }
        titleLabel.text = cellInfo.title
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(){
        addSubViews(views: view, imageView,titleLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.pin(leftAnchor: imageView.trailingAnchor, rightAnchor: view.trailingAnchor, left: 3, right: -3)

        view.centralizeInParent()
        NSLayoutConstraint.activate([
            view.widthAnchor.constraint(equalTo: widthAnchor,constant: -20),
            view.heightAnchor.constraint(equalToConstant: 90),

            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 150),

            titleLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)

        ])
        imageView.bringSubviewToFront(view)
        view.backgroundColor = .systemGray5
        view.layer.cornerRadius = 10
    }
}
