//
//  MovieDetailsCell.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/12/22.
//

import UIKit
import Utilities

class MovieDetailsCell: UICollectionViewCell {
    static var reuseID = String(describing: MovieDetailsCell.self)
    lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.layer.cornerRadius = 10
        image.layer.borderWidth = 0.3
        image.contentMode = .scaleToFill
        return image
    }()

    let titleLabel = TitleLabel(color: .systemGray)
    let overviewLabel = BodyLabel()
    let releaseDateLabel = BodyLabel(textColor: .systemGray, numberOfLines: 1)

    lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, releaseDateLabel])
        stackView.axis = .vertical
        return stackView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemGray6
        configure()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(movie: PreviewData) {
        imageView.kf.indicatorType = .activity
        if let imageURL = movie.imageURL,
           let lowDataURL = movie.lowDataImageURL{
            imageView.kf.setImage(with: imageURL,
                                  options: [.lowDataMode(.network(lowDataURL))])
        } else {
            imageView.image = UIImage(named: kDefaultImageName)
        }
        titleLabel.text = movie.title
        overviewLabel.text = movie.overview
        releaseDateLabel.text = movie.releaseDate
    }

    func configure(){
        addSubViews(views: imageView,stackView,overviewLabel)
        overviewLabel.setContentCompressionResistancePriority(UILayoutPriority(1000), for: .vertical)
        stackView.pin(topAnchor: imageView.topAnchor, leftAnchor: imageView.trailingAnchor, rightAnchor: trailingAnchor, left: 10, top: 3, right: -3)
        imageView.pin(topAnchor: topAnchor, leftAnchor: leadingAnchor, left: 10, top: 5)
        overviewLabel.pin(topAnchor: imageView.bottomAnchor, leftAnchor: imageView.leadingAnchor, bottomAnchor: bottomAnchor, rightAnchor: trailingAnchor, top: 5, bottom: -2, right: -2)
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 110),
            imageView.heightAnchor.constraint(equalToConstant: 160)
        ])
    }
}
