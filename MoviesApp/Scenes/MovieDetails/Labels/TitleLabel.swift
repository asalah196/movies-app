//
//  TitleLabel.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/12/22.
//

import UIKit

class TitleLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    convenience init(textSize: CGFloat = 25, color: UIColor, alignment: NSTextAlignment = .natural){
        self.init(frame: .zero)
        font = UIFont(name: "Helvetica-Bold", size: textSize)
        textColor = color
        numberOfLines = 0
        textAlignment = alignment
    }
}
