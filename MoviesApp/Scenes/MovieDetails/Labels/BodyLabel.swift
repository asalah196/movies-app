//
//  BodyLabel.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/12/22.
//

import UIKit

class BodyLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(textColor: UIColor = .white, numberOfLines: Int=0) {
        super.init(frame: .zero)
        self.textColor = textColor
        configure(textColor ,numberOfLines: numberOfLines)
    }

    private func configure(_ textColor: UIColor = .white, numberOfLines: Int=0){
        self.numberOfLines = numberOfLines
        self.textColor = textColor
        adjustsFontSizeToFitWidth = true
        font = UIFont.preferredFont(forTextStyle: .body)
        adjustsFontForContentSizeCategory = true
        minimumScaleFactor = 0.7
        lineBreakMode = .byTruncatingTail
        translatesAutoresizingMaskIntoConstraints = false
    }
}

