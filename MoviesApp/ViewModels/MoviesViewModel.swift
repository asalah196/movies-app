//
//  MoviesViewModel.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/11/22.
//

import Foundation
import Network

protocol FetchRequestDelegate: AnyObject {
    func fetchDidSucceed()
    func fetchDidFail()
}


class MoviesViewModel {

    private weak var delegate: FetchRequestDelegate?
    private var currentSearchQuery: String?
    private var currentPage = 1
    private var maxPageCount = 0
    private var movies = [MovieDetails]()
    var count: Int {
        movies.count
    }
    var hasNextPage: Bool {
        maxPageCount >= currentPage
    }

    init(delegate: FetchRequestDelegate?=nil) {
        self.delegate = delegate
    }

    func search(for query: String?=nil, forced: Bool=false) {

        var queryCopy = query

        // Here we need to treat the empty string value as nil to fetch trending movies
        if query == "" {
            queryCopy = nil
        }

        // prevent unneeded requests and refreshing
        guard queryCopy != currentSearchQuery || forced else {return}

        currentPage = 1
        let route = getRoute(query: queryCopy, page: currentPage)
        currentSearchQuery = queryCopy
        currentPage+=1

        APIClient.performRequest(route: route) {[weak self] (fetchResult: MoviesAPIResponse?, error) in
            if let error = error {
                print(error)
                self?.delegate?.fetchDidFail()
                return
            }

            guard let fetchResult = fetchResult else {
                print("Unexpectedly received nil")
                return
            }
            if let count = fetchResult.totalPages {
                self?.maxPageCount = count
            }
            self?.movies = fetchResult.results ?? []
            self?.delegate?.fetchDidSucceed()
        }
    }

    func loadMore() {
        guard hasNextPage else {return}
        let route = getRoute(query: currentSearchQuery, page: currentPage)
        currentPage+=1
        APIClient.performRequest(route: route) {[weak self] (fetchResult: MoviesAPIResponse?, error) in
            if let error = error {
                print(error)
                self?.delegate?.fetchDidFail()
                return
            }

            guard let fetchResult = fetchResult else {
                print("Unexpectedly received nil")
                return
            }
            if let count = fetchResult.totalPages {
                self?.maxPageCount = count
            }
            if let movies = fetchResult.results {
                self?.movies.append(contentsOf: movies)
            }
            self?.delegate?.fetchDidSucceed()
        }
    }


    func movie(at ind: Int) -> PreviewData {
        guard ind < movies.count else {
            fatalError("Requested movie out of bounds")
        }
        return movies[ind].previewData
    }

    func shouldLoad(movieOrder: Int) -> Bool {
        return movieOrder == movies.count
    }

}

fileprivate extension MoviesViewModel {
    func getRoute(query: String?=nil, page:Int) -> APIRouter {
        let route: APIRouter!
        if let query = query,
           !query.isEmpty {
            route = APIRouter.fetchMovies(query: query, page: page)
        } else {
            route = APIRouter.popularMovies(page: page)
        }
        return route
    }
}
