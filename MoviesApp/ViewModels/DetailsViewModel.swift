//
//  DetailsViewModel.swift
//  MoviesApp
//
//  Created by Ahmed Salah on 12/11/22.
//

import UIKit
import Network

protocol DetailsViewModelDelegate {
    func viewModel(didFetch movieDetails: PreviewData)
    func viewModel(didFetch similarMovie: [PreviewData])
    func viewModel(didFailWith error: Error?)
}
class DetailsViewModel {
    var delegate: DetailsViewModelDelegate?
    init(delegate: DetailsViewModelDelegate?=nil) {
        self.delegate = delegate
    }

    func fetchMovieDetails(for id: Int) {
        let route = APIRouter.movieDetails(id: id)
        APIClient.performRequest(route: route) { [weak self](response: MovieDetails?, error) in
            if let movieDetails = self?.validateResponse(response: response, error: error) {
                self?.delegate?.viewModel(didFetch: movieDetails.previewData)
            }
        }
    }

    func fetchSimilarMovies(to id: Int) {
        let route = APIRouter.similarMovies(to: id)
        APIClient.performRequest(route: route) { [weak self](response: MoviesAPIResponse?, error) in
            if let response = self?.validateResponse(response: response, error: error) {
                guard let movies = response.results else { return }
                let previewDataFromMovies = movies.map { $0.previewData }
                self?.delegate?.viewModel(didFetch: previewDataFromMovies)
            }
        }
    }

}

extension DetailsViewModel {
    func validateResponse<T: Decodable>(response: T?, error: Error?) -> T? {
        if let error = error {
            print(error)
            delegate?.viewModel(didFailWith: error)
            return nil
        }
        guard let response = response else {
            print("Unexpectedly received nil")
            delegate?.viewModel(didFailWith: nil)
            return nil
        }
        return response
    }
}
