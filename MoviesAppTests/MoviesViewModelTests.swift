//
//  MoviesViewModelTests.swift
//  MoviesAppTests
//
//  Created by Ahmed Salah on 12/15/22.
//

import XCTest
import OHHTTPStubs
@testable import MoviesApp

class MoviesViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearchRequestWhenNilQuery() throws {
        // Given
        let viewModel = MoviesViewModel()
        let matchingExpectation = expectation(description: "Network expectataion")

        // When
        stub(condition: isPath("/3/discover/movie"), response: { _ in
            matchingExpectation.fulfill()
            return HTTPStubsResponse()
        })
        viewModel.search(forced: true)

        // Then
        waitForExpectations(timeout: 2.0)
    }

    func testSearchRequestWhenEmptyStringQuery() throws {
        // Given
        let viewModel = MoviesViewModel()
        let matchingExpectation = expectation(description: "Network expectataion")

        // When
        stub(condition: isPath("/3/discover/movie"), response: { _ in
            matchingExpectation.fulfill()
            return HTTPStubsResponse()
        })
        viewModel.search(for: "", forced: true)

        // Then
        waitForExpectations(timeout: 2.0)
    }

    func testSearchRequestWhenNonEmptyQuery() throws {
        // Given
        let viewModel = MoviesViewModel()
        let matchingExpectation = expectation(description: "Network expectataion")

        // When
        stub(condition: isPath("/3/search/movie"), response: { _ in
            matchingExpectation.fulfill()
            return HTTPStubsResponse()
        })
        viewModel.search(for: "abc", forced: true)

        // Then
        waitForExpectations(timeout: 2.0)
    }


    func testShouldLoadShouldReturnTrue() throws {
        // Given
        let viewModel = MoviesViewModel()

        // Then
        XCTAssertTrue(viewModel.shouldLoad(movieOrder: 0))
    }

    func testShouldLoadShouldReturnFalse() throws {
        // Given
        let viewModel = MoviesViewModel()

        // Then
        XCTAssertFalse(viewModel.shouldLoad(movieOrder: -1))
    }

}
