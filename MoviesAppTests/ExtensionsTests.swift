//
//  ExtensionsTests.swift
//  MoviesAppTests
//
//  Created by Ahmed Salah on 12/15/22.
//

import XCTest
@testable import MoviesApp
class ExtensionsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCellsReuseIds() throws {
        XCTAssertEqual(MovieCell.reuseID, "MovieCell")
        XCTAssertEqual(LoadingCell.reuseID, "LoadingCell")
        XCTAssertEqual(SimilarMovieCell.reuseID, "SimilarMovieCell")
        XCTAssertEqual(MovieDetailsCell.reuseID, "MovieDetailsCell")
    }

}
