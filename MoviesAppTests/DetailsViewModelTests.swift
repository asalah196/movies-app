//
//  DetailsViewModelTests.swift
//  MoviesAppTests
//
//  Created by Ahmed Salah on 12/15/22.
//

import XCTest
import OHHTTPStubs
@testable import MoviesApp
class DetailsViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchMovieDetails() throws {
        // Given
        let viewModel = DetailsViewModel()
        let matchingExpectation = expectation(description: "Network expectataion")

        // When
        stub(condition: isPath("/3/movie/1"), response: { _ in
            matchingExpectation.fulfill()
            return HTTPStubsResponse()
        })
        viewModel.fetchMovieDetails(for: 1)

        // Then
        waitForExpectations(timeout: 2.0)
    }

    func testFetchSimilarMovies() throws {
        // Given
        let viewModel = DetailsViewModel()
        let matchingExpectation = expectation(description: "Network expectataion")

        // When
        stub(condition: isPath("/3/movie/1/similar"), response: { _ in
            matchingExpectation.fulfill()
            return HTTPStubsResponse()
        })
        viewModel.fetchSimilarMovies(to: 1)

        // Then
        waitForExpectations(timeout: 2.0)
    }

}
