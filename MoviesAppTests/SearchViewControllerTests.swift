//
//  MoviesAppTests.swift
//  MoviesAppTests
//
//  Created by Ahmed Salah on 12/8/22.
//

import XCTest
@testable import MoviesApp

class SearchViewControllerTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearchUponReloadingData() throws {
        // Given
        let searchViewController = SearchTableViewController()
        let viewModel = MoviesViewModelMock(delegate: searchViewController)
        searchViewController.viewModel = viewModel
        let searchController = UISearchController()

        // When
        searchViewController.updateSearchResults(for: searchController)

        // Then
        XCTAssertTrue(viewModel.searchCalled)
    }

}
