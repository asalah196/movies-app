//
//  MoviesViewModelMock.swift
//  MoviesAppTests
//
//  Created by Ahmed Salah on 12/15/22.
//

import UIKit
@testable import MoviesApp
class MoviesViewModelMock: MoviesViewModel {

    var searchCalled = false
    override func search(for query: String?, forced: Bool) {
        searchCalled = true
    }

    override func movie(at ind: Int) -> PreviewData {
        PreviewData(id: 1, title: "title", imageURL: nil, lowDataImageURL: nil, releaseDate: "10-01-1996", overview: "overview")
    }

}
